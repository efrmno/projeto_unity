**Development Diary**

Full explanation: https://docs.google.com/presentation/d/1PjleAp9r0PRGvJ_zzzFckSmCzOZk9XZtEcLREpIJxrc/edit?usp=sharing

# Summary

  Project’s Bitbucket url:  https://bitbucket.org/efrmno/projeto_unity/src/master/

------

  Unity ver. Used: 2020.1.4f1
  OS used in the development: Mac OS

------

  Programmer’s Name: Emiliandro Firmino (emiliandrofirmino@gmail.com)
  Programmer’s Linkedin: https://br.linkedin.com/in/emiliandro-firmino-285391109
  Programmer’s Github: https://github.com/Emiliandro

# Tier 0's Code

1. *Unity Canvas’ Button* used to call the movement logic functions, reducing the necessary to code on the Update Method.
2. *Unity Canvas’ Toggle* used to easy test the IA function in game.
3. *Unity Canvas' Scroll Panel* was used to assure the readability of large amounts of text.
4. *Unity’s C# Events* used avoid the centralization of the code and to simplify the maintenance of the code. Example: Health, UI, Score, Game, Obstacle Spawner and Player talk to each other, but there’s no code reference on inspector.
5. *Code overflow* was used to flexible functions of the same name.
6. *Coroutines* were used to control the value update, this way avoiding unnecessary value addition cause by multiframe collisions, for example.
7. *Inheritance* were used to develop objects of same in game behavior but different consequences on collision with the player, that way the same code was reused and the enemies class received extras like an animator to enhance the game’s visuals.
8. All of the three game’s music are always playing, their volume is 0 and 1 depend on the current screen being seen. That way, only one (the relevant music) is listened at time.

# Tier 1's Implementation of 3D and 2D Assets

1. Obstacles, player and part of the scenery.
2. Logos at the main game HUD on a Canvas rendered with “Overlay”
3. Sprite to to simulate part of the scenery
4. Player’s Health bar in a Canvas rendered with ”Space Worlds”


# Tier 1's Implementation of Shaders

1. The normal map was used on the floor, the walls and the cars.
2. The transparent was used on the player body.
3. The self illuminated were applied on the cube coins.

# Tier 1's Collision Detection

1. OnTriggerEnter on “Player”: (1) Obstacles is send back to original world point to wait for the next spawn; (2) Current Coins is changed.
2. OnTriggerEnter on “End of World”: (1) Obstacles is send back to original world point to wait for the next spawn; (2) Obstacles‘ velocity is set to 0.
3. OnTriggerEnter on “Start of World”: (1) Obstacles’ z velocity is setted. (2) Enemies spawning animation is played.

# Tier 1's Particles

1. Used on the lights models to simulate that it’s broken.

# Tier 1's Spawning system

1. The assets are always on the overworlds, that way there’s no recreation of the same assets, avoid unnecessary memory use with onDestroy() and Instatie() functions. The spawner check if the obstacle is on the field, if it’s not, it’s send with a velocity on the z axes so it move. When it’s off camera, the velocity is set to 0 and the game object send to the original position on the overworld to wait for the next spawning call.

# Tier 5's IA

1. For testing purpose it’s enabled via toggle on the in game UI.
2. It uses 5 different rays to detect if there is an obstacle of type “enemie” coming it’s way. If detect, it check the laterals and diagonals to see if is safe to move right or to move left.
