﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    //[SerializeField] private Score score_manager;

    [SerializeField] private GameObject[] obstacles_objects;
    [SerializeField] private Vector3[] spawn_positions;


    public delegate void ObstacleManager();
    public static event ObstacleManager UpdateScore;


    [Tooltip("The default value is 2.0f")]
    private float interval_time;

    [Tooltip("this is the in game clock to call obstacle")]
    private float next_call;

    private int current_spawn_point;

    [SerializeField] private float call_differ;
    private bool isCoroutineExecuting, isRDCoroutineExecuting;

    public void SetDefaultValues()
    {
        Awake();
        ResetAllObjects();
    }


    public void ObstacleSpawnerReset()
    {
        SetDefaultValues();
    }

    private void Awake()
    {
        interval_time = 5.0f;
        next_call = 2.0f;

        current_spawn_point = 0;
        call_differ = 0;

        isCoroutineExecuting = false;
        isRDCoroutineExecuting = false;
    }

    void OnEnable()
    {
        GameManager.CanSpawnObstacle += CanSpawnObstacle;
        GameManager.ObstacleSpawnerReset += ObstacleSpawnerReset;
        HealthManager.EndGame += ResetAllObjects;
        Score.ReduceSpawnDelay += ReduceSpawnInterval;
    }


    void OnDisable()
    {
        GameManager.CanSpawnObstacle -= CanSpawnObstacle;
        GameManager.ObstacleSpawnerReset -= ObstacleSpawnerReset;
        HealthManager.EndGame -= ResetAllObjects;
        Score.ReduceSpawnDelay -= ReduceSpawnInterval;
    }

    private float getInterval() {
        /*
         * when the score is higher, the next call is sooner
         */

        return interval_time - call_differ;
    }

    private void ReduceSpawnInterval() {

        StartCoroutine(SetCallDiffer());
    }

    public IEnumerator SetCallDiffer()
    {
        if (isRDCoroutineExecuting)
            yield break;

        isRDCoroutineExecuting = true;
        call_differ += 1;

        yield return new WaitForSeconds(Constants.AWAIT_SCORE_UPDATE);

        // Code to execute after the delay
        isRDCoroutineExecuting = false;
    }

    public void CanSpawnObstacle()
    {
        if (Time.time >= next_call)
        {
            /*
             * Choose one of GameObjects on list_obstacles
             * then move to front, to towards the player
             */
            setSpawnField();
            next_call += getInterval();
        }

        UpdateInterval();
    }

    protected void UpdateInterval()
    {
        /*
         * Get Score, then change the rules to make the game harder
         */
        interval_time = 2.0f;
    }

    public void ResetAllObjects()
    {
        /*
         * get all obstacles game objects and put on their default position
         * this () is called on the start of a level, on reset and on exit
         */
        foreach (GameObject obstacle in obstacles_objects) {
            obstacle.GetComponent<Obstacle>().ResetObject();
        }
    }


    protected void setSpawnField()
    {
        int which_field = Random.Range(0, spawn_positions.Length);
        if (!current_spawn_point.Equals(which_field))
        {
            //score_manager.UpdateScore(2);
            if (UpdateScore != null) UpdateScore();

            current_spawn_point = which_field;
            Spawn();
        }
        else {
            setSpawnField();
        }

    }

    protected void Spawn()
    {
        int which_one = Random.Range(0, obstacles_objects.Length);

        try
        {

            Obstacle obstacle = obstacles_objects[which_one].GetComponent<Obstacle>();
            bool can_spawn = obstacle.getIs_at_field();

            if (!can_spawn)
            {
                StartCoroutine(setSpawningObstacle(which_one, obstacle));
            }
            else
            {
            }
        }
        catch (UnityException err)
        {
            Debug.Log("Spawn() ->" + err.Message);
        }
    }

    public IEnumerator setSpawningObstacle(int which_one, Obstacle obstacle_to_spawn)
    {
        if (isCoroutineExecuting)
            yield break;

        isCoroutineExecuting = true;

        yield return new WaitForSeconds(Constants.AWAIT_SPAWN_UPDATE);

        GameObject to_spawn = obstacles_objects[which_one];
        to_spawn.transform.position = spawn_positions[current_spawn_point];
        to_spawn.GetComponent<Obstacle>().setIs_at_field(true);
        to_spawn.GetComponent<Obstacle>().setVelocity(obstacle_to_spawn.getDefaultSpeed());

        // Code to execute after the delay
        isCoroutineExecuting = false;
    }


}
