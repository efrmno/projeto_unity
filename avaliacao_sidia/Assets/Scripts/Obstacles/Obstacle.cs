﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Obstacle : MonoBehaviour
{
    [Tooltip("Inform to GameManager if it will enhance score")]
    [SerializeField] private bool is_point;
    [Tooltip("Inform to GameManager if it can be spawned")]
    [SerializeField] private bool is_at_field;
    [SerializeField] private Vector3 default_position;
    //[SerializeField] private Animator animation_manager;
    /*
     * As require component, rigibody is need to make the object move
     */
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float default_speed;

    public void SetDefaultValues()
    {
        rb = GetComponent<Rigidbody>();
        default_speed = 4.5f;
        default_position = transform.position;
    }

    public void Awake()
    {
        SetDefaultValues();
    }

    public bool getIs_point()
    {
        /*
        * if false value: the player will recieve damage and some
        *                 particles will roll on player and UI
        * 
        * if true value: the player will recieve points and the obstacle
        *                will be transparent
         */
        return is_point;
    }

    public bool getIs_at_field()
    {
        /*
        * if false value: the GameManager can spawn it at the field
        * 
        * if true value: the obstacle is already at the field
         */
        return is_at_field;
    }

    public void setIs_at_field(bool value)
    {
        /*
        * if false value: the GameManager can spawn it at the field
        * 
        * if true value: the obstacle is already at the field
         */
        is_at_field = value;
    }

    public void setVelocity(float value)
    {
        /*
         * If go towards player, will use getDefaultSpeed
         * If score is x will use getDefaultSpeed * y
         * If it's on spawn zone, will use 0
         */

        rb.velocity = new Vector3(0, 0, -1 * value);
    }

    public float getDefaultSpeed()
    {
        return default_speed;
    }

    public void ResetObject()
    {
        setVelocity(0);
        is_at_field = false;
        transform.position = default_position;
    }

}
