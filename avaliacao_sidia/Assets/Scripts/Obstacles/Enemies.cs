﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : Obstacle
{
    [SerializeField] private Animator animation_manager;
    [Range(1,2)]
    [SerializeField] private int level;

    public delegate void EnemieManager();

    public static event EnemieManager DamageTypeOne, DamageTypeTwo;

    private void Awake()
    {
        SetDefaultValues();
        if (level != 1 && level != 2) level = 1;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Begin of Map"))
        {
            /*
             * Plays the spawning animation
             */
            animation_manager.Play("EnemieSpawning");
        }

        if (other.tag.Equals("End of Map") || other.tag.Equals("Player"))
        {
            /*
             * set all deafult for next calling
             */
            ResetObject();
        }

        if (other.tag.Equals("Player")) {
            ColWithPlayer();
        }
    }

    private void ColWithPlayer()
    {
        if (level == 1)
        {
            if (DamageTypeOne != null) DamageTypeOne();
        }
        else {
            if (DamageTypeTwo != null) DamageTypeTwo();
        }
    }
}
