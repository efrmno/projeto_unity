﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : Obstacle
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("End of Map") || other.tag.Equals("Player"))
        {
            /*
             * set all deafult for next calling
             */
            ResetObject();
        }
    }
}
