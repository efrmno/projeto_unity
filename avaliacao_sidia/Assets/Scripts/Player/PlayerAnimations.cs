﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimations : MonoBehaviour
{

    [SerializeField] private Animator animator;

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();
    }


    void OnEnable()
    {
        Player.PlayDamage += SetDamage;
    }

    void OnDisable()
    {
        Player.PlayDamage -= SetDamage;
    }


    public void SetDamage()
    {
        /*
         * start or end the animation of damaged based on bool value
         */
        if (true) {
            animator.Play("Player Damage");
        }
    }

}
