﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    
    public delegate void PlayerManager();
    public static event PlayerManager UpdateCoin10, ResetHealth, PlayDamage;


    public void SetDefaultValues()
    {
        transform.position = new Vector3(0, 0, 0);
    }

    private void Awake()
    {
        SetDefaultValues();
    }

    void OnEnable()
    {
        HealthManager.EndGame += SetDefaultValues;
    }


    void OnDisable()
    {
        HealthManager.EndGame -= SetDefaultValues;
    }

    private void OnTriggerEnter(Collider collision)
    {
        string col_tag = collision.tag;
        if (col_tag.Equals("Enemie"))
        {
            if (PlayDamage != null) PlayDamage();
        }

        if (col_tag.Equals("Coin"))
        {
            if (UpdateCoin10 != null) UpdateCoin10();
        }

    }
}
