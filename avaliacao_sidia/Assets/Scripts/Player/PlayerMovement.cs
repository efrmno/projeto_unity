﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private bool isCoroutineExecuting;
    [SerializeField] private int sum_value;

    void OnEnable()
    {
        PlayForMeManager.MoveLeft += MoveLeft;
        PlayForMeManager.MoveRight += MoveRight;
    }


    void OnDisable()
    {
        PlayForMeManager.MoveLeft -= MoveLeft;
        PlayForMeManager.MoveRight -= MoveRight;
    }

    public void Awake()
    {
        isCoroutineExecuting = false;
        sum_value = 2;
    }

    public void CheckUIInputs(bool is_right)
    {
        if (is_right)
        {
            MoveRight();
        }
        else
        {
            MoveLeft();
        }

    }

    public void MoveLeft() {
        StartCoroutine(TransformPlayerPosition(false));
    }

    public void MoveRight() {
        StartCoroutine(TransformPlayerPosition(true));
    }

    public IEnumerator TransformPlayerPosition(bool is_right)
    {
        if (isCoroutineExecuting)
            yield break;

        isCoroutineExecuting = true;

        yield return new WaitForSeconds(Constants.AWAIT_MOVEMENT * Time.deltaTime);
        if (is_right)
        {
            if (transform.position.x != Constants.SPACE_CORNER_RIGHT)
            {
                sum_value = Constants.SPACE_BETWEEN_FIELDS;
            }
            else
            {
                sum_value = 0;
            }
        }
        else
        {
            if (transform.position.x != Constants.SPACE_CORNER_LEFT)
            {
                sum_value = Constants.SPACE_BETWEEN_FIELDS * -1;
            }
            else {
                sum_value = 0;
            }

        }

        transform.position = new Vector3(
            transform.position.x + sum_value,
            Constants.PLAYER_FIXED_Y, Constants.PLAYER_FIXED_Z);

        // Code to execute after the delay
        isCoroutineExecuting = false;
    }

}
