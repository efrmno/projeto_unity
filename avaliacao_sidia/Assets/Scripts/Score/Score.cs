﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private int current_score = 0;
    [SerializeField] private int current_coins = 0;
    [SerializeField] private bool isScoreAddExecuting;
    [SerializeField] private bool isCoinAddExecuting;


    public delegate void ScoreManager();
    public static event ScoreManager ReduceSpawnDelay;

    public void SetDefaultValues()
    {
        current_coins = 0;
        current_score = 0;
        isCoinAddExecuting = false;
        isScoreAddExecuting = false;
    }

    public void ScoreManagerReset()
    {
        SetDefaultValues();
    }

    public void Awake()
    {
        SetDefaultValues();
    }


    void OnEnable()
    {
        GameManager.ScoreManagerReset += ScoreManagerReset;
        ObstacleSpawner.UpdateScore += UpdateScore;
        Player.UpdateCoin10 += UpdateCoinPlusTen;
    }


    void OnDisable()
    {
        GameManager.ScoreManagerReset -= ScoreManagerReset;
        Player.UpdateCoin10 -= UpdateCoinPlusTen;
        ObstacleSpawner.UpdateScore -= UpdateScore;
    }

    public void UpdateCoin(int value)
    {
        StartCoroutine(SetCoins(value));
    }

    public void UpdateCoinPlusTen()
    {
        StartCoroutine(SetCoins(10));
    }

    public void UpdateScore()
    {
        if (current_score == 100 || current_score == 50 ||
            current_score == 20 || current_score == 10) {
            if (ReduceSpawnDelay != null) ReduceSpawnDelay();
        }

        UpdateScore(Constants.DEFAULT_SCORE_ADD);
    }

    public void UpdateScore(int value)
    {
        StartCoroutine(SetScore(value));

    }

    public int GetScore()
    {
        return current_score;
    }


    public int GetCoins()
    {
        return current_coins;
    }

    public IEnumerator SetScore(int value)
    {
        if (isScoreAddExecuting)
            yield break;

        isScoreAddExecuting = true;

        yield return new WaitForSeconds(Constants.AWAIT_SCORE_UPDATE);

        current_score = GetScore() + value;

        // Code to execute after the delay
        isScoreAddExecuting = false;
    }

    public IEnumerator SetCoins(int value)
    {
        if (isCoinAddExecuting)
            yield break;

        isCoinAddExecuting = true;
        current_coins = GetCoins() + value;

        yield return new WaitForSeconds(Constants.AWAIT_SCORE_UPDATE);

        // Code to execute after the delay
        isCoinAddExecuting = false;
    }
}
