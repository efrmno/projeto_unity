﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
    public const int SPACE_BETWEEN_FIELDS = 2;
    public const int SPACE_CORNER_RIGHT = 2;
    public const int SPACE_CORNER_LEFT = -2;

    public const int PLAYER_FIXED_Z = 0;
    public const int PLAYER_FIXED_Y = 0;
    public const int PLAYER_MAX_HP = 6;

    public const float AWAIT_ANIMATION = 1.5f;
    public const float AWAIT_MOVEMENT = 3.0f;
    public const float AWAIT_SCORE_UPDATE = 0.5f;
    public const float AWAIT_SPAWN_UPDATE = 0.75f;

    public const int DEFAULT_SCORE_ADD = 2;
    public const int DEFAULT_COIN_ADD = 10;

    public const int MUSIC_DEAFULT_VOLUME = 7;
}
