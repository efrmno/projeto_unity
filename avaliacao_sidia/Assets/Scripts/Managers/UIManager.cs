﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private GameObject gamePanel;
    [SerializeField] private GameObject infoPanel;
    [SerializeField] private GameObject gameOverPanel;

    [SerializeField] private Text currentScore;
    [SerializeField] private Text gameOverScore;
    [SerializeField] private Text currentCoins;
    [SerializeField] private Text gameOverCoins;

    private void Awake()
    {
        SetPanelsActivesUI(true, false, false, false);
    }

    void OnEnable()
    {
        GameManager.OpenInfoPanelUI += OpenInfoPanelUI;
        GameManager.CloseInfoPanelUI += CloseInfoPanelUI;
        GameManager.SetIsInGameUI += SetIsInGameUI;
        GameManager.SetIsGameOverUI += SetIsGameOverUI;
    }


    void OnDisable()
    {
        GameManager.OpenInfoPanelUI -= OpenInfoPanelUI;
        GameManager.CloseInfoPanelUI -= CloseInfoPanelUI;
        GameManager.SetIsInGameUI -= SetIsInGameUI;
        GameManager.SetIsGameOverUI -= SetIsGameOverUI;
    }

    public void SetCurrentCoinsUI(int value) {
        currentCoins.text = "" + value;
    }

    public void SetGameOverCoinsUI(int value)
    {
        gameOverCoins.text = "" + value;
    }


    public void SetCurrentScoreUI(int value)
    {
        currentScore.text = "" + value;
    }

    public void SetGameOverScoreUI(int value)
    {
        gameOverScore.text = "" + value;
    }

    public void OpenInfoPanelUI()
    {
        SetPanelsActivesUI(false, false, true, false);
    }

    public void CloseInfoPanelUI()
    {
        SetPanelsActivesUI(true, false, false, false);
    }

    public void SetIsInGameUI()
    {
        SetPanelsActivesUI(false, true, false, false);
    }


    public void SetIsGameOverUI()
    {
        SetPanelsActivesUI(false, false, false, true);
    }

    private void SetPanelsActivesUI(bool menu, bool game, bool info, bool dead)
    {
        menuPanel.SetActive(menu);
        gamePanel.SetActive(game);
        infoPanel.SetActive(info);
        gameOverPanel.SetActive(dead);
        ResetCurrents();
    }

    public void ResetCurrents() {
        currentScore.text = "0";
        currentCoins.text = "0";
    }

}
