﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayForMeManager : MonoBehaviour
{
    [SerializeField] private bool drawDebug, isPlayingForMe;

    public delegate void IAManager();
    public static event IAManager MoveLeft, MoveRight;
    private Ray rayMiddle, rayLeft, rayRight, rayRightDiagonal, rayLeftDiagonal;

    public void Awake()
    {
        drawDebug = true;
        isPlayingForMe = false;
    }


    public void ToggleIsPlayingForMe()
    {
        if (isPlayingForMe)
        {
            isPlayingForMe = false;
        }
        else
        {
            isPlayingForMe = true;
        }
    }

    public void GetDebugLines()
    {
        Debug.DrawLine(
            new Vector3(transform.position.x, transform.position.y, transform.position.z),
            new Vector3(transform.position.x, 0, 5), Color.cyan);

        Debug.DrawLine(
            new Vector3(transform.position.x, transform.position.y, transform.position.z),
            new Vector3(2, 0, 0), Color.red);
        Debug.DrawLine(
            new Vector3(transform.position.x, transform.position.y, transform.position.z),
            new Vector3(-2, 0, 0), Color.red);

        Debug.DrawLine(
            new Vector3(transform.position.x, transform.position.y, transform.position.z),
            new Vector3(2, 0, 2), Color.blue);
        Debug.DrawLine(
            new Vector3(transform.position.x, transform.position.y, transform.position.z),
            new Vector3(-2, 0, 2), Color.blue);
    }

    public void Update()
    {
        if (isPlayingForMe)
        {
            /* variables of control */
            RaycastHit hitMiddle, hitLeft, hitRight, hitDiagonalR, hitDiagonalL;

            /*check if is something in front of the player*/
            rayMiddle = new Ray(
                new Vector3(transform.position.x, transform.position.y, transform.position.z),
                new Vector3(transform.position.x, 0, 5));


            if (drawDebug)
            {
                /*development debug lines*/
                GetDebugLines();
            }

            /*middle line dectection*/
            bool RayMiddle = Physics.Raycast(rayMiddle,
                out hitMiddle, 3.0f, LayerMask.GetMask("Enemies"));

            if (RayMiddle)
            {
                /*check laterals to decide where to move*/
                rayLeft = new Ray(
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    new Vector3(2, 0, 0));
                rayRight = new Ray(
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    new Vector3(-2, 0, 0));
                rayLeftDiagonal = new Ray(
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    new Vector3(-2, 0, 2));
                rayRightDiagonal = new Ray(
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    new Vector3(2, 0, 2));

                bool RayRight = Physics.Raycast(rayRight,
                    out hitRight, 3.0f, LayerMask.GetMask("Enemies"));
                bool RayLeft = Physics.Raycast(rayLeft,
                    out hitLeft, 3.0f, LayerMask.GetMask("Enemies"));
                bool RayRightDiagonal = Physics.Raycast(rayRightDiagonal,
                    out hitDiagonalR, 3.0f, LayerMask.GetMask("Enemies"));
                bool RayLeftDiagonal = Physics.Raycast(rayLeftDiagonal,
                    out hitDiagonalL, 3.0f, LayerMask.GetMask("Enemies"));

                if (!RayLeft
                    && (transform.position.x != Constants.SPACE_CORNER_LEFT)
                    && !RayLeftDiagonal)
                {
                    /* if nothing on left, move left */
                    if (MoveLeft != null) MoveLeft();
                }
                else if (!RayRight
                    && (transform.position.x != Constants.SPACE_CORNER_RIGHT)
                    && !RayRightDiagonal)
                {
                    /* if nothing on right, move right */
                    if (MoveRight != null) MoveRight();
                }
            }
        }
    }

}
