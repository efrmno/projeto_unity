﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{


    [SerializeField] private bool isInGame;

    [SerializeField] private Score scoreManager;

    [SerializeField] private int lastScore;
    [SerializeField] private int lastCoins;

    public delegate void GameManagerEvent();

    public static event GameManagerEvent CanSpawnObstacle, OpenInfoPanelUI,
        CloseInfoPanelUI, SetIsInGameUI, SetIsGameOverUI, ObstacleSpawnerReset,
        ScoreManagerReset;


    private void Awake()
    {
        isInGame = false;
    }


    void OnEnable()
    {
        HealthManager.EndGame += EndGameFromPlayer;
    }


    void OnDisable()
    {
        HealthManager.EndGame -= EndGameFromPlayer;
    }


    private void Update()
    {
        if (isInGame)
        {
            UpdateTextScore();
        }
    }

    private void FixedUpdate()
    {
        if (isInGame)
        {
            /* send spawn signal */
            if (CanSpawnObstacle != null) CanSpawnObstacle();

        }
    }


    private void UpdateTextScore()
    {
        int scoreValue = scoreManager.GetScore();
        int coinsValue = scoreManager.GetCoins();

        if (coinsValue != lastCoins)
        {
            lastCoins = coinsValue;
            gameObject.SendMessage("SetCurrentCoinsUI", coinsValue);
            gameObject.SendMessage("SetGameOverCoinsUI", coinsValue);

        }
        if (scoreValue != lastScore)
        {
            lastScore = scoreValue;
            gameObject.SendMessage("SetCurrentScoreUI", scoreValue);
            gameObject.SendMessage("SetGameOverScoreUI", scoreValue);

        }
    }


    public void OpenInfoPanel()
    {
        if (OpenInfoPanelUI != null) OpenInfoPanelUI();
    }

    public void CloseInfoPanel()
    {
        if (CloseInfoPanelUI != null) CloseInfoPanelUI();

    }

    public void SetIsInGame(bool value)
    {
        isInGame = value;
        CallForReset();

        if (!value)
        {
            if (CloseInfoPanelUI != null) CloseInfoPanelUI();
        } else
        {
            if (SetIsInGameUI != null) SetIsInGameUI();
        }

    }

    public void EndGameFromPlayer()
    {
        /*Called only in game over*/
        isInGame = false;
        if (SetIsGameOverUI != null) SetIsGameOverUI();
        CallForReset();
    }

    public bool GetIsInGame()
    {
        return isInGame;
    }

    public void CloseApp()
    {
        Application.Quit();
    }

    public void SetLastCoins(int value)
    {
        lastCoins = value;
    }

    public void setLastScore(int value)
    {
        lastScore = value;
    }


    public void CallForReset()
    {
        if (ObstacleSpawnerReset != null) ObstacleSpawnerReset();
        if (ScoreManagerReset != null) ScoreManagerReset();
    }


}
