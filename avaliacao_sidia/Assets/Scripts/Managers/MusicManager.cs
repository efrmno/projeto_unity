﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioSource in_game;
    [SerializeField] private AudioSource in_menu;
    [SerializeField] private AudioSource game_over;

    private void Awake()
    {
        in_game.volume = 0;
        in_menu.volume = 0;
        game_over.volume = 0;

        StartInMenu();
    }

    void OnEnable()
    {
        HealthManager.StartGameOverMusic += StartGameOver;
    }


    void OnDisable()
    {
        HealthManager.StartGameOverMusic -= StartGameOver;
    }

    public void StartInGame()
    {
        in_menu.volume = 0;
        game_over.volume = 0;

        in_game.Stop();
        in_game.volume = Constants.MUSIC_DEAFULT_VOLUME;
        in_game.Play();
    }

    public void StartInMenu()
    {
        in_game.volume = 0;
        game_over.volume = 0;

        in_menu.Stop();
        in_menu.volume = Constants.MUSIC_DEAFULT_VOLUME;
        in_menu.Play();
    }

    public void StartGameOver()
    {
        in_game.volume = 0;
        in_menu.volume = 0;

        game_over.Stop();
        game_over.volume = Constants.MUSIC_DEAFULT_VOLUME;
        game_over.Play();
    }
}
