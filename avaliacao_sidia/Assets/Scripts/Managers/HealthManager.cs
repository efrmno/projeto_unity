﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    [SerializeField] private GameObject[] playerHealth;
    [SerializeField] private int hp;

    public delegate void HPManager();
    public static event HPManager EndGame, StartGameOverMusic;

    private void Awake()
    {
        ResetHealth();
    }

    private void Update()
    {
        if (hp < 0) {
            if (EndGame != null) EndGame();
            if (StartGameOverMusic != null) StartGameOverMusic();
            ResetHealth();
        }
        
    }

    private void ResetHealth() {
        hp = Constants.PLAYER_MAX_HP;
        UpdateHealthUI(hp);
    }

    void OnEnable()
    {
        Enemies.DamageTypeTwo += Minus2;
        Enemies.DamageTypeOne += Minus1;
    }


    void OnDisable()
    {
        Enemies.DamageTypeTwo -= Minus2;
        Enemies.DamageTypeOne -= Minus1;
    }

    public int GetHealth()
    {
        return hp;
    }

    public void SetHealthItem(int position, int value, int current)
    {
        if (current < value)
        {
            playerHealth[position].SetActive(false);
        }
        else
        {
            playerHealth[position].SetActive(true);
        }
    }

    public void Minus1()
    {
        hp -= 1;
        UpdateHealthUI(hp);
    }

    public void Minus2()
    {
        hp -= 2;
        UpdateHealthUI(hp);
    }

    private void UpdateHealthUI(int player_health)
    {
        for (int i = 0; i < playerHealth.Length; i++)
        {
            SetHealthItem(i, i + 1, player_health);
        }
    }
}